﻿using System;

namespace StudentFeature
{

    public class Passport
    {
        public int Id { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public DateTime Birthday { get; set; }

        public bool Sex { get; set; }


        public virtual Person Person { get; set; }
    }
}
