﻿namespace StudentFeature
{
    public class Student : Person
    {
        public int Id { get; set; }

        public string Course { get; set; }

        public virtual CreditCard CreditCard { get; set; }
        public virtual University University { get; set; }
        public virtual Backpack Backpack { get; set; }
    }
}
