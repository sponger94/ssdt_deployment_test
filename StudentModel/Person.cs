﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StudentFeature
{
    public abstract class Person
    {
        public int Id { get; set; }

        public virtual Passport Passport { get; set; }
    }
}
