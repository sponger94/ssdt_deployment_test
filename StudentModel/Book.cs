﻿namespace StudentFeature
{
    public class Book
    {
        public int Id { get; set; }

        public string Title { get; set; }

        public string Description { get; set; }

        public string AuthorName { get; set; }

        public int? Backpack_Id { get; set; }

        public virtual Backpack Backpack { get; set; }
    }
}
