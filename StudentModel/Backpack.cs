﻿using System.Collections.Generic;

namespace StudentFeature
{
    public class Backpack
    {
        public int Id { get; set; }

        public virtual Student Student { get; set; }

        public virtual List<Book> Books { get; set; }
    }
}
