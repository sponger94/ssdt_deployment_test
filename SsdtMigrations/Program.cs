﻿using System;
using Microsoft.SqlServer.Dac.Compare;
using System.Data.SqlClient;
using MyOtherDeploymentContributor;
using Microsoft.SqlServer.Dac;

namespace SsdtMigrations
{
    class Program
    {
        static void Main(string[] args)
        {
            var dacFile = @"D:\DB\ef_code_first_migration.dacpac";
            var sourceDacpac = new SchemaCompareDacpacEndpoint(dacFile);

            var csb = new SqlConnectionStringBuilder();
            csb.DataSource = "localhost";
            csb.InitialCatalog = "ef_code_first_migration";
            csb.IntegratedSecurity = true;
            
            var targetDatabase = new SchemaCompareDatabaseEndpoint(csb.ToString());

            var comparison = new SchemaComparison(sourceDacpac, targetDatabase);

            // Persist comparison file to disk in Schema Compare (.scmp) format
            comparison.SaveToFile(@"D:\DB\mycomparison.scmp");

            // Load comparison from Schema Compare (.scmp) file
            comparison = new SchemaComparison(@"D:\DB\mycomparison.scmp");
            comparison.Options.BlockOnPossibleDataLoss = false;
            comparison.Options.AdditionalDeploymentContributors = CreditCardScriptContributor.ContributorId;


            SchemaComparisonResult comparisonResult = comparison.Compare();
            var dacServices = new DacServices(csb.ToString());
            var dacpac = DacPackage.Load(dacFile, DacSchemaModelStorageType.Memory);
            var script = dacServices.GenerateDeployScript(dacpac, csb.InitialCatalog, comparison.Options);

            // Find the change to table1 and exclude it.
            foreach (SchemaDifference difference in comparisonResult.Differences)
            {
                if (difference.TargetObject.Name != null &&
                    difference.TargetObject.Name.HasName &&
                    difference.TargetObject.Name.Parts[1] == "table1")
                {
                    comparisonResult.Exclude(difference);
                    break;
                }

                if(difference.UpdateAction == SchemaUpdateAction.Add)
                {
                    Console.WriteLine("Add");
                    
                    
                }
                else if(difference.UpdateAction == SchemaUpdateAction.Change)
                {
                    Console.WriteLine("Change");
                }
                else
                {
                    Console.WriteLine("Delete");
                }
            }
            // Publish the changes to the target database
            //comparisonResult.TargetModel.AddOrUpdateObjects("", "", new Microsoft.SqlServer.Dac.Model.TSqlObjectOptions());

            SchemaComparePublishResult publishResult = comparisonResult.PublishChangesToTarget();

            Console.WriteLine(publishResult.Success ? "Publish succeeded." : "Publish failed.");
            Console.Read();
        }
    }
}
