﻿using EFCodeFirstMigrationsResearch.Data;
using StudentFeature;
using System;

namespace EFCodeFirstMigrationsResearch
{
    class Program
    {
        static void Main(string[] args)
        {
            Disclaimer();

            Log("Creating context...");

            var context = new Context();
            context.Books.Create(new Book { AuthorName = "George Martin", Description = "Todo do do do do do do", Title = "The song of the Ice and Fire." });
            //context.Students.Create(new Student { Course = "1" });

            //Log("Wiping out");
            //context.Books.Delete(b => true);
            //context.Backpacks.Delete(b => true);
            //context.Passports.Delete(p => true);
            //context.Students.Delete(s => true);

            context.SaveChanges();
            
            Log("Done!");

            Console.ReadLine();
        }

        private static void Disclaimer()
        {
            Log("Eagle Test Database Upgrader [Version 1.0]");
            Log("This is the test project for EF code migrations.");
            Log();
        }

        private static void Log(string text = "")
        {
            Console.WriteLine(text);
        }
    }
}
