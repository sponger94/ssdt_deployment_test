﻿using StudentFeature;

namespace EFCodeFirstMigrationsResearch.Data
{
    public interface IBookRepository : IRepository<Book>
    {
        Book GetBy(int id);

        Book GetBy(string title);
    }
}
