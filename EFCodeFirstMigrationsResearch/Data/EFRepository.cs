﻿using System;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;

namespace EFCodeFirstMigrationsResearch.Data
{
    public class EFRepository<T> : IRepository<T> where T : class
    {
        protected DbContext Context;
        protected readonly bool ShareContext;

        public EFRepository(DbContext context) : this(context, false) { }

        public EFRepository(DbContext context, bool shareContext)
        {
            Context = context;
            ShareContext = shareContext;
        }

        protected DbSet<T> DbSet => Context.Set<T>();

        public IQueryable<T> All() => DbSet.AsQueryable();

        public bool Any(Expression<Func<T, bool>> predicate) => DbSet.Any(predicate);

        public int Count => DbSet.Count();

        public T Create(T t)
        {
            DbSet.Add(t);

            SaveChanges();

            return t;
        }

        public int Delete(T t)
        {
            DbSet.Remove(t);

            SaveChanges();

            return 0;
        }

        public int Delete(Expression<Func<T, bool>> predicate)
        {
            var records = FindAll(predicate);

            foreach (var record in records)
                DbSet.Remove(record);

            SaveChanges();

            return 0;
        }

        public void Dispose()
        {
            if (ShareContext || Context == null)
                return;

            try
            {
                Context.Dispose();
            }
            catch
            {
                //Do nothing
            }
        }

        public T Find(params object[] keys) => DbSet.Find(keys);

        public T Find(Expression<Func<T, bool>> predicate) => DbSet.SingleOrDefault(predicate);

        public IQueryable<T> FindAll(Expression<Func<T, bool>> predicate) => DbSet.Where(predicate).AsQueryable();

        public IQueryable<T> FindAll(Expression<Func<T, bool>> predicate, int index, int size)
        {
            var skip = index * size;

            IQueryable<T> query = DbSet;

            if (predicate != null)
                query = query.Where(predicate);

            if (skip != 0)
                query.Skip(skip);

            return query.Take(size).AsQueryable();
        }

        public int Update(T t)
        {
            var entry = Context.Entry(t);
            DbSet.Attach(t);
            entry.State = EntityState.Modified;
            SaveChanges();

            return 0;
        }

        protected void SaveChanges()
        {
            if (!ShareContext)
                Context.SaveChanges();
        }
    }
}
