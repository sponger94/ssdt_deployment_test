﻿using StudentFeature;

namespace EFCodeFirstMigrationsResearch.Data
{
    public interface IUnviersityRepository : IRepository<University>
    {
        University GetBy(int id);
    }
}
