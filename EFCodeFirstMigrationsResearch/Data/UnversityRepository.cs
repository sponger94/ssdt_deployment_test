﻿using StudentFeature;
using System;
using System.Data.Entity;

namespace EFCodeFirstMigrationsResearch.Data
{
    public class UnversityRepository : EFRepository<University>, IUnviersityRepository
    {
        public UnversityRepository(DbContext context) : base(context) { }
        public UnversityRepository(DbContext context, bool shareContext) : base(context, shareContext) { }

        public University GetBy(int id) => Find(u => u.Id == id);
    }
}
