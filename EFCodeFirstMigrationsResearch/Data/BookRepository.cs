﻿using StudentFeature;
using System.Data.Entity;

namespace EFCodeFirstMigrationsResearch.Data
{
    public class BookRepository : EFRepository<Book>, IBookRepository
    {
        public BookRepository(DbContext context) : base(context) { }
        public BookRepository(DbContext context, bool shareContext) : base(context, shareContext) { }

        public Book GetBy(int id) => Find(book => book.Id == id);
        public Book GetBy(string title) => Find(book => book.Title == title);
    }
}
