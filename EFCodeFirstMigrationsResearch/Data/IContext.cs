﻿using System;

namespace EFCodeFirstMigrationsResearch.Data
{
    public interface IContext : IDisposable
    {
        IBackpackRepository Backpacks { get; }
        IBookRepository Books { get; }
        IPassportRepository Passports { get; }
        IStudentRepository Students { get; }
        int SaveChanges();
    }
}
