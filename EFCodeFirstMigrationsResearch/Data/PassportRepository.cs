﻿using StudentFeature;
using System.Data.Entity;

namespace EFCodeFirstMigrationsResearch.Data
{
    public class PassportRepository : EFRepository<Passport>, IPassportRepository
    {
        public PassportRepository(DbContext context) : base(context) { }
        public PassportRepository(DbContext context, bool shareContext) : base(context, shareContext) { }

        public Passport GetBy(int id) => Find(passport => passport.Id == id);
        public Passport GetBy(string firstName) => Find(passport => passport.FirstName == firstName);
        public Passport GetBy(string firstName, string lastName) => Find(passport => passport.FirstName == firstName && passport.LastName == lastName);
    }
}
