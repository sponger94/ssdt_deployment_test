﻿using StudentFeature;

namespace EFCodeFirstMigrationsResearch.Data
{
    public interface IStudentRepository : IRepository<Student>
    {
        Student GetBy(int id);
    }
}
