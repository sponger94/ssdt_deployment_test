﻿using System.Data.Entity;

namespace EFCodeFirstMigrationsResearch.Data
{
    public class Context : IContext
    {
        private readonly DbContext _db;

        public IBackpackRepository Backpacks { get; private set; }

        public IBookRepository Books { get; private set; }

        public IPassportRepository Passports { get; private set; }

        public IStudentRepository Students { get; private set; }

        public IUnviersityRepository Universities { get; private set; }

        public Context(DbContext context = null, IBackpackRepository backpacks = null, IBookRepository books = null, IPassportRepository passports = null, IStudentRepository students = null)
        {
            _db = context ?? new StudentDatabase();
            Backpacks = backpacks ?? new BackpackRepository(_db, true);
            Books = books ?? new BookRepository(_db, true);
            Passports = passports ?? new PassportRepository(_db, true);
            Students = students ?? new StudentRepository(_db, true);
        }

        public void Dispose()
        {
            if (_db == null)
                return;

            try
            {
                _db.Dispose();
            }
            catch
            {
                //Do nothing
            }
        }

        public int SaveChanges()
        {
            return _db.SaveChanges();
        }
    }
}
