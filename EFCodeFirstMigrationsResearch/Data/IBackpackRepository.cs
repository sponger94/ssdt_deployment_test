﻿using StudentFeature;

namespace EFCodeFirstMigrationsResearch.Data
{
    public interface IBackpackRepository : IRepository<Backpack>
    {
        Backpack GetBy(int id);
    }
}
