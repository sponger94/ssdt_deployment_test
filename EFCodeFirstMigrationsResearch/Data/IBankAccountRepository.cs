﻿using StudentFeature.Banking;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EFCodeFirstMigrationsResearch.Data
{
    public interface IBankAccountRepository : IRepository<BankAccount>
    {
        BankAccount GetBy(int id);
    }
}
