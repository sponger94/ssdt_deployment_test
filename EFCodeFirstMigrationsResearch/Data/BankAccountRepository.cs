﻿using StudentFeature.Banking;
using System;
using System.Data.Entity;

namespace EFCodeFirstMigrationsResearch.Data
{
    public class BankAccountRepository : EFRepository<BankAccount>, IBankAccountRepository
    {
        public BankAccountRepository(DbContext context) : base(context) { }
        public BankAccountRepository(DbContext context, bool shareContext) : base(context, shareContext) { }

        public BankAccount GetBy(int id) => Find(ba => ba.Id == id);
    }
}
