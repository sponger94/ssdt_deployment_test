﻿using StudentFeature;
using System.Data.Entity;

namespace EFCodeFirstMigrationsResearch.Data
{
    public class StudentDatabase : DbContext
    {
        public DbSet<Backpack> Backpacks { get; set; }
        public DbSet<Book> Books { get; set; }
        public DbSet<Passport> Passports { get; set; }
        public DbSet<Student> Student { get; set; }

        public DbSet<University> Universities { get; set; }

        public StudentDatabase() : base("data source=localhost;initial catalog=ef_code_first_migration;integrated security=True;")
        {
           
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Backpack>()
                .HasKey(backpack => backpack.Id)
                .HasMany(backpack => backpack.Books)
                .WithOptional(book => book.Backpack)
                .HasForeignKey(b => b.Backpack_Id);

            //modelBuilder.Entity<Book>()
            //    .HasKey(book => book.Id)
            //    .HasOptional(book => book.Backpack);

            modelBuilder.Entity<Passport>()
                .HasKey(passport => passport.Id)
                .HasOptional(passport => passport.Person)
                .WithOptionalDependent(person => person.Passport);

            modelBuilder.Entity<University>()
               .HasKey(unversity => unversity.Id)
               .HasMany(unversity => unversity.Students)
               .WithOptional(student => student.University);

            //modelBuilder.Entity<Student>()
            //    .HasKey(student => student.Id)
            //    .HasOptional(student => student.Passport)
            //    .WithRequired(passport => passport.Student);

            modelBuilder.Entity<Backpack>()
                .HasOptional(backpack => backpack.Student)
                .WithOptionalDependent(student => student.Backpack);

            modelBuilder.Entity<Student>()
                .HasOptional(student => student.CreditCard)
                .WithOptionalDependent(card => card.Student);

            base.OnModelCreating(modelBuilder);
        }
    }
}
