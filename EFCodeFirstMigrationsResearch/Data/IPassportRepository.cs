﻿using StudentFeature;

namespace EFCodeFirstMigrationsResearch.Data
{
    public interface IPassportRepository : IRepository<Passport>
    {
        Passport GetBy(int id);

        Passport GetBy(string firstName);

        Passport GetBy(string firstName, string lastName);
    }
}
