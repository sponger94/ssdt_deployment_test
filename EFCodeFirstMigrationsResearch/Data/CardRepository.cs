﻿using System;
using StudentFeature.Banking.Cards;
using System.Data.Entity;

namespace EFCodeFirstMigrationsResearch.Data
{
    public class CardRepository : EFRepository<Card>, ICardRepository
    {
        public CardRepository(DbContext context) : base(context) { }
        public CardRepository(DbContext context, bool shareContext) : base(context, shareContext) { }

        public Card GetBy(int id) => Find(card => card.Id == id);
    }
}
