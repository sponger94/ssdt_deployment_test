﻿using StudentFeature.Banking.Cards;

namespace EFCodeFirstMigrationsResearch.Data
{
    public interface ICardRepository : IRepository<Card> 
    {
        Card GetBy(int id);
    }
}
