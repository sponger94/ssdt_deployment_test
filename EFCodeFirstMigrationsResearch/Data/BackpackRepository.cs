﻿using StudentFeature;
using System.Data.Entity;

namespace EFCodeFirstMigrationsResearch.Data
{
    public class BackpackRepository : EFRepository<Backpack>, IBackpackRepository
    {
        public BackpackRepository(DbContext context) : base(context) { }
        public BackpackRepository(DbContext context, bool shareContext) : base(context, shareContext) { }

        public Backpack GetBy(int id) => Find(b => b.Id == id);
    }
}
