﻿using StudentFeature;
using System.Data.Entity;

namespace EFCodeFirstMigrationsResearch.Data
{
    public class StudentRepository : EFRepository<Student>, IStudentRepository
    {
        public StudentRepository(DbContext context) : base(context) { }
        public StudentRepository(DbContext context, bool shareContext) : base(context, shareContext) { }

        public Student GetBy(int id) => Find(student => student.Id == id);
    }
}
