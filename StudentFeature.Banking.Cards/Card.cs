﻿namespace StudentFeature.Banking.Cards
{
    public class Card
    {
        public int Id { get; set; }
        public double Balance { get; set; }
        public string CardHolder { get; set; }
        public string CardInfo { get; set; }

        public string CardNumber { get; set; }

        public virtual BankAccount BankAccount { get; set; }
    }
}
