﻿namespace StudentFeature.Banking
{
    public class BankAccount
    {
        public int Id { get; set; }
        public string AccountNumber { get; set; }
        public string RoutingNumber { get; set; }

        public virtual Student Student { get; set; }
    }
}
